package ru.edu.project.frontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.notification.NotificationService;
import ru.edu.project.backend.api.user.UserInfo;
import ru.edu.project.backend.api.user.UserService;

@Controller
@RequestMapping("/profile")
public class ProfileController {


    /**
     * Зависимость на сервис пользователя.
     */
    @Autowired
    private UserService userService;

    /**
     * Зависимость на сервис нотфикации.
     */
    @Autowired
    private NotificationService notificationService;

    /**
     * Просмотр профиля.
     *
     * @param model
     * @param authentication
     * @return viewName
     */
    @GetMapping("/")
    public String view(final Model model, final Authentication authentication) {
        if (authentication == null) {
            return "redirect:/";
        }

        UserInfo user = getUserId(authentication);

        model.addAttribute("email", user.getEmail());
        model.addAttribute("emailConfirmed", user.getEmailConfirmed());

        return "profile/view";
    }

    /**
     * Обновление email.
     *
     * @param email
     * @param authentication
     * @return redirect
     */
    @PostMapping("/updateEmail")
    public String updateEmail(@RequestParam("email") final String email, final Authentication authentication) {
        Long id = getId(authentication);
        return "redirect:/profile/?emailChanged=" + userService.updateEmail(id, email);
    }

    /**
     * Активация почты.
     *
     * @param userId
     * @param code
     * @param model
     * @return viewName
     */
    @GetMapping("/activateProfile/{userId}/{confirmCode}")
    public String activate(@PathVariable("userId") final Long userId, @PathVariable("confirmCode") final String code, final Model model) {
        model.addAttribute("success", notificationService.confirmEmail(userId, code));
        return "profile/activation";
    }

    private UserInfo getUserId(final Authentication authentication) {
        Long id = getId(authentication);
        return userService.findById(id);
    }

    private Long getId(final Authentication authentication) {
        if (authentication == null) {
            throw new IllegalStateException("not authorized");
        }
        UserDetailsId userDetailsId = (UserDetailsId) authentication.getPrincipal();
        return userDetailsId.getUserId();
    }

}
