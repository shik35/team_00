package ru.edu.project.backend.api.notification;

import ru.edu.project.backend.api.requests.RequestInfo;

public interface NotificationService {

    /**
     * Сгенерировать код подтверждения и отправить на email.
     *
     * @param userId
     */
    void sendConfirmationCode(Long userId);

    /**
     * Подтвердить email по коду.
     *
     * @param userId
     * @param code
     * @return isSuccess
     */
    boolean confirmEmail(Long userId, String code);

    /**
     * Отправить сообщение пользователю.
     *
     * @param notifyMessageRequest
     */
    void sendNotification(NotifyMessageRequest notifyMessageRequest);

    /**
     * Отправка сообщения о готовности заказа.
     *
     * @param req
     */
    void sendReadyMessage(RequestInfo req);
}
