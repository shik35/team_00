package ru.edu.project.backend.api.user;

import ru.edu.project.backend.api.common.AcceptorArgument;

/**
 * Интерфейс работы с пользователями.
 */
public interface UserService {

    /**
     * Регистрация пользователя.
     *
     * @param userInfo
     * @return id
     */
    @AcceptorArgument
    Long register(UserInfo userInfo);

    /**
     * Получение данных о пользователе.
     *
     * @param username
     * @return username
     */
    UserInfo loadUserByUsername(String username);

    /**
     * Обновление данных о пользователе.
     *
     * @param userInfo
     * @return updatedUserInfo
     */
    @AcceptorArgument
    UserInfo update(UserInfo userInfo);

    /**
     * Поиск по userId.
     *
     * @param id
     * @return userInfo
     */
    UserInfo findById(Long id);

    /**
     * Обновление email.
     *
     * @param id
     * @param email
     * @return true
     */
    boolean updateEmail(Long id, String email);
}
