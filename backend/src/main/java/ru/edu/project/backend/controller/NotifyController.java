package ru.edu.project.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.notification.NotificationService;
import ru.edu.project.backend.api.notification.NotifyMessageRequest;
import ru.edu.project.backend.api.requests.RequestInfo;
import ru.edu.project.backend.service.NotifyServiceLayer;

@RestController
@RequestMapping("/notify")
public class NotifyController implements NotificationService {

    /**
     * Логгер.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(NotifyController.class);
    /**
     * Зависимость на делегата.
     */
    @Autowired
    private NotifyServiceLayer delegate;

    /**
     * Сгенерировать код подтверждения и отправить на email.
     *
     * @param userId
     */
    @Override
    @GetMapping("/sendConfirmationCode/{userId}")
    public void sendConfirmationCode(@PathVariable("userId") final Long userId) {
        delegate.sendConfirmationCode(userId);
    }

    /**
     * Подтвердить email по коду.
     *
     * @param userId
     * @param code
     * @return isSuccess
     */
    @Override
    @GetMapping("/confirmEmail/{userId}/{code}")
    public boolean confirmEmail(@PathVariable("userId") final Long userId, @PathVariable("code") final String code) {
        try {
            return delegate.confirmEmail(userId, code);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }

    /**
     * Отправить сообщение пользователю.
     *
     * @param notifyMessageRequest
     */
    @Override
    @PostMapping("/sendNotification")
    public void sendNotification(@RequestBody final NotifyMessageRequest notifyMessageRequest) {
        delegate.sendNotification(notifyMessageRequest);
    }

    /**
     * Отправка сообщения о готовности заказа.
     *
     * @param req
     */
    @Override
    @PostMapping("/sendReadyMessage")
    public void sendReadyMessage(@RequestBody final RequestInfo req) {
        delegate.sendReadyMessage(req);
    }
}
