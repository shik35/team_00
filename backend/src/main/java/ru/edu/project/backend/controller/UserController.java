package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.user.UserInfo;
import ru.edu.project.backend.api.user.UserService;
import ru.edu.project.backend.service.UserServiceLayer;

@RestController
@RequestMapping("/user")
public class UserController implements UserService {

    /**
     * Зависимость на делегата.
     */
    @Autowired
    private UserServiceLayer delegate;

    /**
     * Регистрация пользователя.
     *
     * @param userInfo
     * @return id
     */
    @Override
    @PostMapping("/register")
    public Long register(@RequestBody final UserInfo userInfo) {
        return delegate.register(userInfo);
    }

    /**
     * Получение данных о пользователе.
     *
     * @param username
     * @return username
     */
    @Override
    @GetMapping("/loadUserByUsername/{username}")
    public UserInfo loadUserByUsername(@PathVariable("username") final String username) {
        return delegate.loadUserByUsername(username);
    }

    /**
     * Обновление данных о пользователе.
     *
     * @param userInfo
     * @return updatedUserInfo
     */
    @Override
    @PostMapping("/update")
    public UserInfo update(@RequestBody final UserInfo userInfo) {
        return delegate.update(userInfo);
    }

    /**
     * Поиск по userId.
     *
     * @param id
     * @return userInfo
     */
    @Override
    @GetMapping("/findById/{id}")
    public UserInfo findById(@PathVariable("id") final Long id) {
        return delegate.findById(id);
    }

    /**
     * Обновление email.
     *
     * @param id
     * @param email
     * @return true
     */
    @Override
    @GetMapping("/updateEmail/{userId}/{email}")
    public boolean updateEmail(@PathVariable("userId") final Long id, @PathVariable("email") final String email) {
        return delegate.updateEmail(id, email);
    }
}
