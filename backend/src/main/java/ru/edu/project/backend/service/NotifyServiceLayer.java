package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.notification.NotificationService;
import ru.edu.project.backend.api.notification.NotifyMessage;
import ru.edu.project.backend.api.notification.NotifyMessageRequest;
import ru.edu.project.backend.api.requests.RequestInfo;
import ru.edu.project.backend.api.user.UserInfo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@Service
public class NotifyServiceLayer implements NotificationService {

    /**
     * Топик для сообщений.
     */
    public static final String SERVICES_NOTIFY_TOPIC = "services.notify";

    /**
     * Шаблон ссылки для активации.
     */
    public static final String ACTIVATE_LINK_TEMPLATE = "http://localhost:8080/profile/activateProfile/%d/%s";

    /**
     * Атрибут в модели сообщения.
     */
    public static final String USERNAME_ATTR = "username";
    /**
     * Атрибут в модели сообщения.
     */
    public static final String ACTIVATION_LINK_ATTR = "activationLink";

    /**
     * Заголовок письма.
     */
    public static final String CONFIRMATION_SUBJECT = "Подтверждение адреса";

    /**
     * Код шаблона письма.
     */
    public static final String CONFIRMATION_TEMPLATE_ID = "confirmationCode";

    /**
     * Заголовок письма.
     */
    public static final String READY_SUBJECT = "Заказ готов к выдаче";

    /**
     * Зависимость на шаблон kafka publisher.
     */
    @Autowired
    private KafkaTemplate<String, NotifyMessage> kafkaTemplate;

    /**
     * Форматирование даты.
     */
    private static final  DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");


    /**
     * Зависимость на сервис пользователей.
     * Циклическая зависимость.
     */
    @Autowired
    @Lazy
    private UserServiceLayer userServiceLayer;


    /**
     * Сгенерировать код подтверждения и отправить на email.
     *
     * @param userId
     */
    @Override
    public void sendConfirmationCode(final Long userId) {
        UserInfo user = getUser(userId);

        if (user.getEmailConfirmed()) {
            throw new IllegalStateException("user already confirm email");
        }

        user.setEmailCode(UUID.randomUUID().toString());
        user.setEmailConfirmed(false);
        userServiceLayer.update(user);

        HashMap<String, Object> model = new HashMap<>();
        model.put(USERNAME_ATTR, user.getUsername());
        model.put(ACTIVATION_LINK_ATTR, String.format(ACTIVATE_LINK_TEMPLATE, userId, user.getEmailCode()));

        sendNotificationMessage(NotifyMessage.builder()
                .email(user.getEmail())
                .subject(CONFIRMATION_SUBJECT)
                .templateId(CONFIRMATION_TEMPLATE_ID)
                .model(model)
                .build());
    }

    /**
     * Подтвердить email по коду.
     *
     * @param userId
     * @param code
     * @return isSuccess
     */
    @Override
    public boolean confirmEmail(final Long userId, final String code) {
        UserInfo user = getUser(userId);

        if (code == null || code.isEmpty() || user.getEmailCode() == null || user.getEmailCode().isEmpty() || !code.equals(user.getEmailCode())) {
            return false;
        }

        user.setEmailCode("ok");
        user.setEmailConfirmed(true);
        userServiceLayer.update(user);
        return true;
    }

    /**
     * Отправка сообщения о готовности заказа.
     *
     * @param req
     */
    @Override
    public void sendReadyMessage(final RequestInfo req) {
        UserInfo user = getUser(req.getClientId());

        HashMap<String, Object> model = new HashMap<>();
        model.put("requestId", req.getId());
        model.put("requestCreateDate", DATE_FORMAT.format(new Date(req.getCreatedAt().getTime())));


        sendNotificationMessage(NotifyMessage.builder()
                .email(user.getEmail())
                .subject(READY_SUBJECT)
                .templateId("requestReady")
                .model(model)
                .build());
    }

    /**
     * Отправить сообщение пользователю.
     *
     * @param notifyMessageRequest
     */
    @Override
    public void sendNotification(final NotifyMessageRequest notifyMessageRequest) {
        UserInfo user = getUser(notifyMessageRequest.getUserId());

        sendNotificationMessage(NotifyMessage.builder()
                .email(user.getEmail())
                .subject(CONFIRMATION_SUBJECT)
                .templateId(notifyMessageRequest.getTemplateId())
                .model(notifyMessageRequest.getModel())
                .build());
    }

    private UserInfo getUser(final Long userId) {
        UserInfo user = userServiceLayer.findById(userId);
        if (user == null) {
            throw new IllegalArgumentException("user not found");
        }
        return user;
    }

    private void sendNotificationMessage(final NotifyMessage notifyMessage) {
        kafkaTemplate.send(SERVICES_NOTIFY_TOPIC, notifyMessage);
    }
}
