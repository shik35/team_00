package ru.edu.project.notify.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.notification.NotifyMessage;
import ru.edu.project.notify.service.EmailService;

@RestController
public class NotifyController {

    /**
     * Зависимость.
     */
    @Autowired
    private EmailService emailService;

    /**
     * Отладка отправки почты.
     *
     * @param message
     * @return true
     */
    @PostMapping("/send")
    public boolean sendEmail(@RequestBody final NotifyMessage message) {
        emailService.process(message);
        return true;
    }

}
