package ru.edu.project.notify.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.notification.NotifyMessage;
import ru.edu.project.notify.service.EmailService;

@Service
public class NotifyMessageListener {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(NotifyMessageListener.class);

    /**
     * Зависимость на сервис.
     */
    @Autowired
    private EmailService emailService;

    /**
     * Слушатель сообщений из топика.
     *
     * @param notifyMessage
     */
    @KafkaListener(topics = "services.notify")
    public void listener(@Payload final NotifyMessage notifyMessage) {
        LOGGER.info("Получено сообщение: {}", notifyMessage.getTemplateId());
        emailService.process(notifyMessage);
    }
}
